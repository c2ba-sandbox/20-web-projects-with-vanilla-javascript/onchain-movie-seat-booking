# On Chain Movie Seat Booking

This project in an implementation of the [Movie Seat Booking Tutorial from Traversy Media](https://vanillawebprojects.com/) with additions to train myself in typescript and solidity development.

Here is the plan:

- Implement the frontend by following the tutorial and use typescript instead of javascript
- Refactor and improve the code if necessary
- Setup a truffle environment for smart contract development
- Implement tests and smart contract(s) to handle the application data (movie list, booking seats, etc)
- Connect the frontend to a deployed instance of the smart contract (locally at first)
- Implement CI/CD to execute test automatically and deploy to live environments (Mumbai testnet and Polygon)
- Implement CI/CD to build the frontend with connection to live environments and deploy to gitlab pages

Once all of this is done, consider implementing advanced features such as staking, liquidity providing for the cinema, movie voting, seats secondary market place, auctions, etc.
