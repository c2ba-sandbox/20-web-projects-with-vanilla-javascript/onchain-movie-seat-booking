pragma solidity >=0.8.0 <0.9.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/DecentralizedMovieTheater.sol";

contract TestDecentralizedMovieTheater {
  uint public initialBalance = 50 ether;

  DecentralizedMovieTheater theater = DecentralizedMovieTheater(DeployedAddresses.DecentralizedMovieTheater());

  string expectedTitle = "A New Hope";
  uint256 expectedTicketPrice = 0.42 ether;
  uint256 movieId;

  uint256[] wantedSeatsFirstBatch;
  uint256[] wantedSeatsSecondBatch;
  uint256[] wantedSeatsThirdBatch;

  uint256[] bookedSeatIdsFirstBatch;

  address self = address(this);

  function beforeAll() public {
    wantedSeatsFirstBatch.push(0);
    wantedSeatsFirstBatch.push(12);
    wantedSeatsFirstBatch.push(46);
    wantedSeatsFirstBatch.push(47);

    wantedSeatsSecondBatch.push(8);
    wantedSeatsSecondBatch.push(4);

    wantedSeatsThirdBatch.push(16);
    wantedSeatsThirdBatch.push(3);
    wantedSeatsThirdBatch.push(31);
  }

  function testUserCanAddMovie() public {
    uint256 currentBalance = self.balance;
    require(currentBalance > theater.addMovieCost());

    movieId = theater.addMovie{value:theater.addMovieCost()}(expectedTitle, expectedTicketPrice);
    Assert.equal(movieId, 0, "Movie should be the first one");

    (string memory title, uint256 ticketPrice, uint256 bookedSeatCount) = theater.movies(movieId);
    Assert.equal(title, expectedTitle, "Title should match");
    Assert.equal(ticketPrice, expectedTicketPrice, "Ticket price should match");
    Assert.equal(bookedSeatCount, 0, "No seats should have been booked");

    Assert.equal(theater.movieToOwner(movieId), address(this), "Movie owner should be this contract");

    Assert.equal(self.balance, currentBalance - theater.addMovieCost(), "Movie addition should cost theater.addMovieCost");
  }

  function _testPayingLessThanTheCostOfAddMovieFails() public {
    // todo: fix, seems to fail even when we send enough ETH
    (bool r, ) = address(theater).call{value:theater.addMovieCost() - 1}(abi.encodePacked(theater.addMovie.selector, expectedTitle, expectedTicketPrice));
    Assert.isFalse(r, "Paying less than the cost of add movie should fail");
  }

  function testPayingMoreThanTheCostOfAddMovieGivesMoneyBack() public {
    // Note: this test would fail if the test contract does not define a receive() function to get ether back
    uint256 currentBalance = self.balance;
    theater.addMovie{value:theater.addMovieCost() + 1}(expectedTitle, expectedTicketPrice);
    Assert.equal(self.balance, currentBalance - theater.addMovieCost(), "Paying more than the cost should give money back");
  }

  function testUserCanBookSeats() public {
    uint256 currentBalance = self.balance;
    uint256 totalCost = expectedTicketPrice * wantedSeatsFirstBatch.length;
    bookedSeatIdsFirstBatch = theater.bookSeats{value:totalCost}(movieId, wantedSeatsFirstBatch);

    Assert.equal(bookedSeatIdsFirstBatch.length, wantedSeatsFirstBatch.length, "Number of booked seat ids should match input");
    for (uint256 i = 0; i < bookedSeatIdsFirstBatch.length; ++i) {
      Assert.equal(bookedSeatIdsFirstBatch[i], i, "Seats should be the first ones booked");
    }
    Assert.equal(self.balance, currentBalance - totalCost, "Seats booking should cost number of seats time individual price");

    (uint256[] memory bookedSeatIdsGetValue, DecentralizedMovieTheater.Seat[] memory bookedSeats) = theater.getBookedSeats(movieId);
    Assert.equal(bookedSeatIdsGetValue.length, wantedSeatsFirstBatch.length, "Number of booked seats ids for this movie should match");
    Assert.equal(bookedSeats.length, wantedSeatsFirstBatch.length, "Number of booked seats for this movie should match");
    for (uint256 i = 0; i < bookedSeatIdsGetValue.length; ++i) {
      Assert.equal(bookedSeatIdsGetValue[i], bookedSeatIdsFirstBatch[i], "Seat ids should match");
      Assert.equal(bookedSeats[i].movieId, movieId, "Movie id for seat should match");
      Assert.equal(bookedSeats[i].index, wantedSeatsFirstBatch[i], "Seat id for seat should match");
    }
  }

  function testUserCanBookMoreSeats() public {
    uint256 totalCost = expectedTicketPrice * wantedSeatsSecondBatch.length;
    uint256[] memory bookedSeatIdsSecondBatch = theater.bookSeats{value:totalCost}(movieId, wantedSeatsSecondBatch);

    Assert.equal(bookedSeatIdsSecondBatch.length, wantedSeatsSecondBatch.length, "Number of booked seat ids should match input");
    for (uint256 i = 0; i < bookedSeatIdsSecondBatch.length; ++i) {
      Assert.equal(bookedSeatIdsSecondBatch[i], i + wantedSeatsFirstBatch.length, "Seats should be the second ones booked");
    }

    (uint256[] memory bookedSeatIdsGetValue, DecentralizedMovieTheater.Seat[] memory bookedSeats) = theater.getBookedSeats(movieId);
    Assert.equal(
      bookedSeatIdsGetValue.length, wantedSeatsFirstBatch.length + wantedSeatsSecondBatch.length,
      "Number of booked seats ids for this movie should match"
    );
    Assert.equal(
      bookedSeats.length, wantedSeatsFirstBatch.length + wantedSeatsSecondBatch.length,
      "Number of booked seats for this movie should match"
    );
    for (uint256 i = 0; i < wantedSeatsFirstBatch.length; ++i) {
      Assert.equal(bookedSeatIdsGetValue[i], bookedSeatIdsFirstBatch[i], "Seat ids should match");
      Assert.equal(bookedSeats[i].movieId, movieId, "Movie id for seat should match");
      Assert.equal(bookedSeats[i].index, wantedSeatsFirstBatch[i], "Seat id for seat should match");
    }
    for (uint256 i = 0; i < wantedSeatsSecondBatch.length; ++i) {
      Assert.equal(bookedSeatIdsGetValue[i + wantedSeatsFirstBatch.length], bookedSeatIdsSecondBatch[i], "Seat ids should match");
      Assert.equal(bookedSeats[i + wantedSeatsFirstBatch.length].movieId, movieId, "Movie id for seat should match");
      Assert.equal(bookedSeats[i + wantedSeatsFirstBatch.length].index, wantedSeatsSecondBatch[i], "Seat id for seat should match");
    }
  }

  function _testUserCannotBookAlreadyBookedSeat() public {
    // todo: fix, seems to succeed but should not
    uint256 totalCost = expectedTicketPrice * wantedSeatsSecondBatch.length;
    (bool r, ) = address(theater).call{value:totalCost}(abi.encodePacked(theater.bookSeats.selector, movieId, wantedSeatsSecondBatch));
    Assert.isFalse(r, "User should not be able to book already booked seats");
  }

  function testPayingMoreForSeatsGivesMoneyBack() public {
    uint256 currentBalance = self.balance;
    uint256 totalCost = expectedTicketPrice * wantedSeatsThirdBatch.length;
    theater.bookSeats{value:totalCost + 1}(movieId, wantedSeatsThirdBatch);
    Assert.equal(self.balance, currentBalance - totalCost,  "Paying more than the cost for seats should give money back");
  }

  function afterAll() public {
    // Don't waste ether, give it back to sender address
    payable(msg.sender).transfer(self.balance);
  }

  receive() external payable {}
}
