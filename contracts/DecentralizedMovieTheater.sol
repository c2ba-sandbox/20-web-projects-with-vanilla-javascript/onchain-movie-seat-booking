// SPDX-License-Identifier: Unlicense
pragma solidity >=0.8.0 <0.9.0;

contract DecentralizedMovieTheater {
  struct Movie {
    string title;
    uint256 ticketPrice;
    uint256 bookedSeatCount;
  }
  event NewMovieAdded(uint256 movieId, address owner, Movie movie);

  struct Seat {
      uint256 movieId;
      uint256 index; // Index of seat in room
  }
  event NewSeatBooked(uint256 seatId, address owner, Seat seat);

  Movie[] public movies;
  mapping (uint256 => address) public movieToOwner;

  mapping (uint256 => mapping (uint256 => bool)) public movieToSeats;

  Seat[] public seats;
  mapping (uint256 => address) public seatToOwner;

  uint256 public addMovieCost = 0.5 ether;
  uint256 public seatCount = 48;

  function addMovie(string memory _title, uint256 _ticketPrice) public payable returns (uint256) {
    require(msg.value >= addMovieCost);
    movies.push(Movie(_title, _ticketPrice, 0));
    uint256 movieId = movies.length - 1;
    movieToOwner[movieId] = msg.sender;
    if (msg.value > addMovieCost) {
      payable(msg.sender).transfer(msg.value - addMovieCost);
    }
    emit NewMovieAdded(movieId, msg.sender, movies[movieId]);
    return movieId;
  }

  function getMovies() public view returns (Movie[] memory) {
    return movies;
  }

  function getMovieCount() public view returns (uint256) {
    return movies.length;
  }

  function _bookSeat(uint256 _movieId, uint256 _seatIndex) internal returns (uint256) {
    require(_seatIndex < seatCount);
    require(movieToSeats[_movieId][_seatIndex] == false);
    ++movies[_movieId].bookedSeatCount;
    movieToSeats[_movieId][_seatIndex] = true;
    seats.push(Seat(_movieId, _seatIndex));
    uint256 seatId = seats.length - 1;
    seatToOwner[seatId] = msg.sender;
    emit NewSeatBooked(seatId, msg.sender, seats[seatId]);
    return seatId;
  }

  function bookSeats(uint256 _movieId, uint256[] calldata _seatIndices) public payable returns (uint256[] memory) {
    require(_movieId < movies.length);
    Movie memory movie = movies[_movieId];
    uint256 totalCost = movie.ticketPrice * _seatIndices.length;
    require(msg.value >= totalCost);
    uint256[] memory seatIds = new uint256[](_seatIndices.length);
    for (uint256 seatIdx = 0; seatIdx < _seatIndices.length; ++seatIdx) {
      seatIds[seatIdx] = _bookSeat(_movieId, _seatIndices[seatIdx]);
    }
    if (msg.value > totalCost) {
      payable(msg.sender).transfer(msg.value - totalCost);
    }
    return seatIds;
  }

  function getBookedSeats(uint256 _movieId) external view returns (uint256[] memory movieSeatIds, Seat[] memory movieSeats) {
    movieSeatIds = new uint256[](movies[_movieId].bookedSeatCount);
    movieSeats = new Seat[](movies[_movieId].bookedSeatCount);
    uint256 counter = 0;
    for (uint256 i = 0; i < seats.length; ++i) {
      if (seats[i].movieId == _movieId) {
        movieSeatIds[counter] = i;
        movieSeats[counter] = seats[i];
        ++counter;
      }
    }
  }
}
