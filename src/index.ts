class App {
  web3 = null;
  theaterContract = null;

  scene = document.querySelector(".scene") as HTMLElement;
  seatContainer = document.querySelector(".seats") as HTMLElement;
  count = document.getElementById("count") as HTMLElement;
  total = document.getElementById("total") as HTMLElement;
  movieSelect = document.getElementById("movie") as HTMLSelectElement;
  paySeatsButton = document.getElementById(
    "pay-seats-button",
  ) as HTMLButtonElement;
  payMovieButton = document.getElementById(
    "pay-movie-button",
  ) as HTMLButtonElement;

  movies = [
    {
      title: "Avengers: Endgame",
      ticketPrice: 0.1,
    },
    {
      title: "Joker",
      ticketPrice: 0.12,
    },
    {
      title: "Toy Story",
      ticketPrice: 0.8,
    },
    {
      title: "Star Wars",
      ticketPrice: 0.9,
    },
  ];

  seatRowCount = 6;
  seatPerRowCount = 8;

  selectedSeats = new Set() as Set<number>;

  constructor() {
    this.init();
  }

  async init() {
    await this.initWeb3();
    await this.initContract();
    this.bindEvents();
    await this.updateUI();
  }

  async initWeb3() {
    if (!window.ethereum) {
      // Only handle modern dapp browsers
      document.getElementById("content").innerHTML =
        "Please install a modern wallet extension.";
      return;
    }
    try {
      await window.ethereum.request({ method: "eth_requestAccounts" });
    } catch (error) {
      document.getElementById("content").innerHTML =
        "Please connect with wallet extension.";
      return;
    }
    this.web3 = new ethers.providers.Web3Provider(window.ethereum);
  }

  async initContract() {
    const data = await (
      await fetch("contracts/DecentralizedMovieTheater.json")
    ).json();

    const theaterContract = TruffleContract(data);
    theaterContract.setProvider(window.ethereum);
    const theaterAddress = (await theaterContract.deployed()).address;

    this.theaterContract = new ethers.Contract(
      theaterAddress,
      data["abi"],
      this.web3,
    );
    this.theaterContract = this.theaterContract.connect(this.web3.getSigner());
  }

  bindEvents() {
    window.ethereum.on("accountsChanged", async (accounts) => {
      await this.updateUI();
    });
    this.scene.addEventListener("click", (e) => {
      const target = e.target as HTMLElement;
      if (!target.matches(".seat:not(.occupied)")) {
        return;
      }
      this.handleSeatSelection(target);
    });
    this.movieSelect.addEventListener("change", (e) => {
      this.selectedSeats = new Set();
      this.updateUI();
    });
    this.paySeatsButton.addEventListener("click", async (e) => {
      const { selectedSeatsCount, totalPrice } =
        this.computeTransactionParameters();
      const selectedMovie = Math.max(this.movieSelect.selectedIndex, 0);
      await this.theaterContract.bookSeats(
        selectedMovie,
        Array.from(this.selectedSeats),
        {
          value: ethers.utils.parseEther(`${totalPrice}`),
        },
      );
      this.selectedSeats = new Set();
    });
    this.payMovieButton.addEventListener("click", async (e) => {
      const movieTitle = (
        document.getElementById("movie-title") as HTMLInputElement
      ).value;
      const movieTicketPrice = (
        document.getElementById("movie-ticket-price") as HTMLInputElement
      ).value;
      await this.theaterContract.addMovie(
        movieTitle,
        ethers.utils.parseEther(movieTicketPrice),
        {
          value: await this.theaterContract.addMovieCost(),
        },
      );
    });
    this.theaterContract.on(
      "NewMovieAdded",
      (
        movieId: ethers.BigNumber,
        ownerAddress: string,
        {
          title,
          ticketPrice,
        }: { title: string; ticketPrice: ethers.BigNumber },
      ) => {
        console.log(
          movieId.toNumber(),
          ownerAddress,
          title,
          ethers.utils.formatEther(ticketPrice.toString()),
        );
        this.updateUI();
      },
    );
    this.theaterContract.on(
      "NewSeatBooked",
      (
        seatId: ethers.BigNumber,
        ownerAddress: string,
        {
          movieId,
          index,
        }: { movieId: ethers.BigNumber; index: ethers.BigNumber },
      ) => {
        console.log(
          seatId.toNumber(),
          ownerAddress,
          movieId.toNumber(),
          index.toNumber(),
        );
        if (this.selectedSeats.has(index)) {
          this.selectedSeats.delete(index);
        }
        this.updateUI();
      },
    );
  }

  handleSeatSelection(target: HTMLElement) {
    const seatId = parseInt(target.dataset["id"] as string);
    if (this.selectedSeats.has(seatId)) {
      this.selectedSeats.delete(seatId);
    } else {
      this.selectedSeats.add(seatId);
    }
    this.updateUI();
  }

  computeTransactionParameters() {
    const selectedSeatsCount = this.selectedSeats.size;
    const ticketPrice = parseFloat(this.movieSelect.value) || 0;
    return {
      ticketPrice,
      selectedSeatsCount,
      totalPrice: ticketPrice * selectedSeatsCount,
    };
  }

  async updateUI() {
    const selectedMovie = Math.max(this.movieSelect.selectedIndex, 0);

    const movies = await this.theaterContract.getMovies();
    const movieCount = movies.length;

    if (movieCount > 0) {
      this.movieSelect.innerHTML = "".concat(
        ...movies.map(({ title, ticketPrice }) => {
          ticketPrice = ethers.utils.formatEther(ticketPrice);
          return `<option value="${ticketPrice}">${title} (${ticketPrice} MATIC)</option>`;
        }),
      );
      this.movieSelect.style.display = "inline";
      (document.getElementById("no-movie-added") as HTMLElement).style.display =
        "none";
    } else {
      this.movieSelect.style.display = "none";
    }
    this.movieSelect.selectedIndex = selectedMovie;

    this.seatContainer.innerHTML = "".concat(
      ...new Array(this.seatRowCount).fill(0).map((_, rowIdx) => {
        const rowHtml = "".concat(
          ...new Array(this.seatPerRowCount).fill(0).map((_, seatIdx) => {
            const id = rowIdx * this.seatPerRowCount + seatIdx;
            return `<div class="seat" data-id="${id}"></div>`;
          }),
        );
        return `<div class="row">${rowHtml}</div>`;
      }),
    );

    const { selectedSeatsCount, totalPrice } =
      this.computeTransactionParameters();

    this.count.innerText = `${selectedSeatsCount}`;
    this.total.innerText = `${totalPrice.toFixed(2)}`;

    if (movieCount === 0) {
      this.paySeatsButton.innerText = "No Movie Yet";
      this.paySeatsButton.disabled = true;
    } else if (selectedSeatsCount === 0) {
      this.paySeatsButton.innerText = "Select seats";
      this.paySeatsButton.disabled = true;
    } else {
      this.paySeatsButton.innerText = `Pay ${totalPrice.toFixed(
        2,
      )} MATIC for ${selectedSeatsCount} seats`;
      this.paySeatsButton.disabled = false;
    }

    const { movieSeatIds, movieSeats } =
      await this.theaterContract.getBookedSeats(selectedMovie);

    const occupiedSeats = movieSeats.map(
      ({ index }: { index: ethers.BigNumber }) => index.toNumber(),
    );

    const allSeats = document.querySelectorAll(".scene .seat");
    occupiedSeats.forEach((occupiedSeat) => {
      allSeats[occupiedSeat].classList.add("occupied");
    });

    this.selectedSeats.forEach((selectedSeat) => {
      allSeats[selectedSeat].classList.add("selected");
    });

    const addMoviePrice = ethers.utils.formatEther(
      (await this.theaterContract.addMovieCost()).toString(),
    );

    document.getElementById(
      "add-movie-price",
    ).innerText = `(Pay ${addMoviePrice} MATIC)`;
  }
}

(() => {
  const app = new App();
})();
